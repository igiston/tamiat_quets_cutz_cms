/* This is the Firebase configuration file */

import firebase from 'firebase'
import config from './config'

require('firebase/firestore')

let app = firebase.initializeApp(config)
let db = app.database()

const firestore = firebase.firestore()

// create a database references
const settingsRef = db.ref('settings')
const mediaRef = db.ref('media')
const navRef = db.ref('nav')
const contentsRef = db.ref('contents')
const fieldsRef = db.ref('fields')

const routesRef = db.ref('routes')
export { settingsRef, mediaRef, navRef, routesRef, contentsRef, fieldsRef, db, firestore }
